package copy

import (
	"fmt"
	"io/ioutil"
	"testing"
)

const path = `../files_test`

func createFile(name string, data string) error {
	fileBytes := []byte(data)
	err := ioutil.WriteFile(getFilePath(name), fileBytes, 0644)
	return err
}

func getFilePath(name string) string {
	return fmt.Sprintf(`%s/%s`, path, name)
}
func TestCopyOffset(t *testing.T) {
	var (
		from    = getFilePath("from.txt")
		fromStr = "sample text for checking a copy text project"
		to      = getFilePath("to_offset.txt")
		limit   = 0
		offset  = 2
	)

	err := createFile(from, fromStr)
	if err != nil {
		t.Fatalf("error on prepare source file")
	}

	err = Copy(from, to, limit, offset)
	if err != nil {
		t.Fatalf("error on copy file")
	}

	targetBytes, err := ioutil.ReadFile(to)
	if err != nil {
		t.Fatalf("error on read target file")
	}
	targetStr := string(targetBytes)
	if targetStr != "mple text for checking a copy text project" {
		t.Fatalf("bad data in target file after copy")
	}
}

func TestCopyLimit(t *testing.T) {
	var (
		from    = getFilePath("from.txt")
		fromStr = "sample text for checking a copy text project"
		to      = getFilePath("to_limit.txt")
		limit   = 6
		offset  = 0
	)

	err := createFile(from, fromStr)
	if err != nil {
		t.Fatalf("error on prepare source file")
	}

	err = Copy(from, to, limit, offset)
	if err != nil {
		t.Fatalf("error on copy file")
	}

	targetBytes, err := ioutil.ReadFile(to)
	if err != nil {
		t.Fatalf("error on read target file")
	}
	targetStr := string(targetBytes)
	if targetStr != "sample" {
		t.Fatalf("bad data in target file after copy")
	}
}

func TestLimitRange(t *testing.T) {
	var (
		from    = getFilePath("from.txt")
		fromStr = "sample text for checking a copy text project"
		to      = getFilePath("to_limit_offset.txt")
		limit   = 4
		offset  = 2
	)

	err := createFile(from, fromStr)
	if err != nil {
		t.Fatalf("error on prepare source file")
	}

	err = Copy(from, to, limit, offset)
	if err != nil {
		t.Fatalf("error on copy file")
	}

	targetBytes, err := ioutil.ReadFile(to)
	if err != nil {
		t.Fatalf("error on read target file")
	}
	targetStr := string(targetBytes)

	if targetStr != "mple" {
		t.Fatalf("bad data in target file after copy")
	}
}

func TestFullCopy(t *testing.T) {
	var (
		from    = getFilePath(`from.txt`)
		fromStr = "sample text for checking a copy text project"
		to      = getFilePath("to_all.txt")
		limit   = 0
		offset  = 0
	)

	err := createFile(from, fromStr)
	if err != nil {
		t.Fatalf("error on prepare source file")
	}

	err = Copy(from, to, limit, offset)
	if err != nil {
		t.Fatalf("error on copy file")
	}

	targetBytes, err := ioutil.ReadFile(to)
	if err != nil {
		t.Fatalf("error on read target file")
	}
	targetStr := string(targetBytes)
	if targetStr != fromStr {
		t.Fatalf("bad data in target file after copy")
	}
}
