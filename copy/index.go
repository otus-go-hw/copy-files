package copy

import (
	"fmt"
	"github.com/cheggaaa/pb"
	"io"
	"os"
)

func Copy(from string, to string, limit int, offset int) (err error) {
	file, err := os.Open(from)
	if err != nil {
		return err
	}
	defer file.Close()
	sourceFileInfo, err := file.Stat()
	if err != nil {
		return err
	}
	sourceFileSize := sourceFileInfo.Size()
	if sourceFileSize <= int64(offset) {
		return fmt.Errorf("offset is too big, max %v", sourceFileSize-1)
	}
	if offset > 0 {
		_, err = file.Seek(int64(offset), 0)
		if err != nil {
			return err
		}
	}

	barSize := sourceFileSize
	if limit != 0 {
		barSize = int64(limit)
	}
	bar := pb.Full.Start64(barSize)
	sourceReader := bar.NewProxyReader(file)

	targetFile, err := os.Create(to)
	if err != nil {
		return err
	}
	defer targetFile.Close()
	if limit != 0 {
		_, err = io.CopyN(targetFile, sourceReader, int64(limit))
	} else {
		_, err = io.Copy(targetFile, sourceReader)
	}
	bar.Finish()
	return err
}
